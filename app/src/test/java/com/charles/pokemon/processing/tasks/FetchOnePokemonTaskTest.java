package com.charles.pokemon.processing.tasks;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonMap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyZeroInteractions;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.reflect.Whitebox.getInternalState;
import static org.powermock.reflect.Whitebox.setInternalState;

import java.io.IOException;
import java.util.function.Consumer;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.charles.pokemon.model.data.ComplexPokemon;
import com.charles.pokemon.processing.util.JsonFetcher;
import com.charles.pokemon.processing.util.PokemonParser;

@RunWith(PowerMockRunner.class)
@PrepareForTest(value = { JsonFetcher.class, PokemonParser.class })
public final class FetchOnePokemonTaskTest
{
    private static final String POKEMON_URL = "pokemonUrl";

    private FetchOnePokemonTask testee;
    private Consumer<ComplexPokemon> onSuccess;
    private Runnable onError;

    @Before
    public void setUp()
    {
        onSuccess = mock(Consumer.class);
        onError = mock(Runnable.class);
        testee = new FetchOnePokemonTask(POKEMON_URL, onSuccess, onError);
        mockStatic(JsonFetcher.class);
        mockStatic(PokemonParser.class);
    }

    @Test
    public void shouldHandleDoInBackgroundWhenSuccess() throws Exception
    {
        ComplexPokemon expected = pokemon();
        when(JsonFetcher.fetchJson(POKEMON_URL)).thenReturn("json");
        when(PokemonParser.parseOnePokemon("json")).thenReturn(expected);

        ComplexPokemon actual = testee.doInBackground();

        assertThat(getSuccess()).isTrue();
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void shouldHandleDoInBackgroundWhenFailure() throws Exception
    {
        when(JsonFetcher.fetchJson(POKEMON_URL)).thenThrow(new IOException());

        ComplexPokemon actual = testee.doInBackground();

        assertThat(getSuccess()).isFalse();
        assertThat(actual).isNull();
    }

    @Test
    public void shouldHandleOnPostExecuteWhenSuccess()
    {
        ComplexPokemon expected = pokemon();
        setSuccess(true);

        testee.onPostExecute(expected);

        verify(onSuccess).accept(expected);
        verifyZeroInteractions(onError);
    }

    @Test
    public void shouldHandleOnPostExecuteWhenFailure()
    {
        ComplexPokemon expected = pokemon();
        setSuccess(false);

        testee.onPostExecute(expected);

        verifyZeroInteractions(onSuccess);
        verify(onError).run();
    }

    private ComplexPokemon pokemon()
    {
        return new ComplexPokemon("bulbasaur", 69, 70,
                asList("grass", "poison"), singletonMap("hp", 45));
    }

    private boolean getSuccess()
    {
        return getInternalState(testee, "success");
    }

    private void setSuccess(boolean value)
    {
        setInternalState(testee, "success", value);
    }
}