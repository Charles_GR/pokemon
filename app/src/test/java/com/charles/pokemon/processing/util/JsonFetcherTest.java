package com.charles.pokemon.processing.util;

import static com.charles.pokemon.processing.util.JsonFetcher.fetchJson;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.inOrder;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;
import static org.powermock.reflect.Whitebox.setInternalState;

import java.io.IOException;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(value = { JsonFetcher.class, Request.class, Response.class, ResponseBody.class })
public final class JsonFetcherTest
{
    private Request.Builder requestBuilder;

    @Before
    public void setUp() throws Exception
    {
        requestBuilder = mockRequestBuilder();
    }

    @Test
    public void shouldReturnJsonWhenFetchingJsonFromUrlIsASuccess() throws Exception
    {
        OkHttpClient client = mockClient(200, "json");
        setInternalState(JsonFetcher.class, "CLIENT", client);

        String json = fetchJson("url");

        InOrder inOrder = inOrder(requestBuilder);
        inOrder.verify(requestBuilder).url("url");
        inOrder.verify(requestBuilder).build();
        assertThat(json).isEqualTo("json");
    }

    @Test(expected = IOException.class)
    public void shouldThrowExceptionWhenFetchingJsonFromUrlIsAFailure() throws Exception
    {
        OkHttpClient client = mockClient(500, "error");
        setInternalState(JsonFetcher.class, "CLIENT", client);

        fetchJson("url");
    }

    private static Request.Builder mockRequestBuilder() throws Exception
    {
        Request.Builder requestBuilder = Mockito.mock(Request.Builder.class);
        when(requestBuilder.url(anyString())).thenReturn(requestBuilder);
        Request request = mock(Request.class);
        when(requestBuilder.build()).thenReturn(request);
        whenNew(Request.Builder.class).withNoArguments().thenReturn(requestBuilder);
        return requestBuilder;
    }

    private static OkHttpClient mockClient(int code, String body) throws Exception
    {
        OkHttpClient client = mock(OkHttpClient.class);
        Call call = mockCall(code, body);
        when(client.newCall(any())).thenReturn(call);
        return client;
    }

    private static Call mockCall(int code, String body) throws Exception
    {
        Call call = mock(Call.class);
        Response response = mockResponse(code, body);
        when(call.execute()).thenReturn(response);
        return call;
    }

    private static Response mockResponse(int code, String body) throws Exception
    {
        Response response = mock(Response.class);
        ResponseBody responseBody = mockResponseBody(body);
        when(response.code()).thenReturn(code);
        when(response.body()).thenReturn(responseBody);
        return response;
    }

    private static ResponseBody mockResponseBody(String body) throws Exception
    {
        ResponseBody responseBody = mock(ResponseBody.class);
        when(responseBody.string()).thenReturn(body);
        return responseBody;
    }
}