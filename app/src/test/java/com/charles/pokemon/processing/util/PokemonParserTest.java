package com.charles.pokemon.processing.util;

import static com.charles.pokemon.processing.util.PokemonParser.parseAllPokemon;
import static com.charles.pokemon.processing.util.PokemonParser.parseOnePokemon;
import static java.nio.charset.Charset.defaultCharset;
import static org.apache.commons.io.IOUtils.resourceToString;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

import java.util.List;

import org.junit.Test;

import com.charles.pokemon.model.data.ComplexPokemon;
import com.charles.pokemon.model.data.SimplePokemon;

public final class PokemonParserTest
{
    @Test
    public void shouldParseAllPokemonFromJson() throws Exception
    {
        String json = resourceToString("/all_pokemon.json", defaultCharset());

        List<SimplePokemon> pokemon = parseAllPokemon(json);

        assertThat(pokemon).hasSize(3);
        assertThat(pokemon.get(0)).isEqualToComparingFieldByField(
                new SimplePokemon("bulbasaur", "https://pokeapi.co/api/v2/pokemon/1/"));
        assertThat(pokemon.get(1)).isEqualToComparingFieldByField(
                new SimplePokemon("ivysaur", "https://pokeapi.co/api/v2/pokemon/2/"));
        assertThat(pokemon.get(2)).isEqualToComparingFieldByField(
                new SimplePokemon("venusaur", "https://pokeapi.co/api/v2/pokemon/3/"));
    }

    @Test
    public void shouldParseOnePokemonFromJson() throws Exception
    {
        String json = resourceToString("/one_pokemon.json", defaultCharset());

        ComplexPokemon pokemon = parseOnePokemon(json);

        assertThat(pokemon.getName()).isEqualTo("bulbasaur");
        assertThat(pokemon.getWeight()).isEqualTo(6.9f);
        assertThat(pokemon.getHeight()).isEqualTo(70);
        assertThat(pokemon.getTypes()).containsExactly("grass", "poison");
        assertThat(pokemon.getStats()).containsExactly(
                entry("hp", 45), entry("attack", 49), entry("defense", 49), entry("special-attack", 65),
                entry("special-defense", 65), entry("speed", 45));
    }
}