package com.charles.pokemon.processing.tasks;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyZeroInteractions;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.reflect.Whitebox.getInternalState;
import static org.powermock.reflect.Whitebox.setInternalState;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.charles.pokemon.model.data.SimplePokemon;
import com.charles.pokemon.processing.util.JsonFetcher;
import com.charles.pokemon.processing.util.PokemonParser;

@RunWith(PowerMockRunner.class)
@PrepareForTest(value = { JsonFetcher.class, PokemonParser.class })
public final class FetchAllPokemonTaskTest
{
    private static final String JSON_URL = "https://pokeapi.co/api/v2/pokemon?limit=300";

    private FetchAllPokemonTask testee;
    private Consumer<List<SimplePokemon>> onSuccess;
    private Runnable onError;

    @Before
    public void setUp()
    {
        onSuccess = mock(Consumer.class);
        onError = mock(Runnable.class);
        testee = new FetchAllPokemonTask(onSuccess, onError);
        mockStatic(JsonFetcher.class);
        mockStatic(PokemonParser.class);
    }

    @Test
    public void shouldHandleDoInBackgroundWhenSuccess() throws Exception
    {
        List<SimplePokemon> expected = pokemon();
        when(JsonFetcher.fetchJson(JSON_URL)).thenReturn("json");
        when(PokemonParser.parseAllPokemon("json")).thenReturn(expected);

        List<SimplePokemon> actual = testee.doInBackground();

        assertThat(getSuccess()).isTrue();
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void shouldHandleDoInBackgroundWhenFailure() throws Exception
    {
        when(JsonFetcher.fetchJson(JSON_URL)).thenThrow(new IOException());

        List<SimplePokemon> actual = testee.doInBackground();

        assertThat(getSuccess()).isFalse();
        assertThat(actual).isEmpty();
    }

    @Test
    public void shouldHandleOnPostExecuteWhenSuccess()
    {
        List<SimplePokemon> expected = pokemon();
        setSuccess(true);

        testee.onPostExecute(expected);

        verify(onSuccess).accept(expected);
        verifyZeroInteractions(onError);
    }

    @Test
    public void shouldHandleOnPostExecuteWhenFailure()
    {
        List<SimplePokemon> expected = pokemon();
        setSuccess(false);

        testee.onPostExecute(expected);

        verifyZeroInteractions(onSuccess);
        verify(onError).run();
    }

    private List<SimplePokemon> pokemon()
    {
        return asList(
                new SimplePokemon("bulbasaur", "https://pokeapi.co/api/v2/pokemon/1/"),
                new SimplePokemon("ivysaur", "https://pokeapi.co/api/v2/pokemon/2/")
        );
    }

    private boolean getSuccess()
    {
        return getInternalState(testee, "success");
    }

    private void setSuccess(boolean value)
    {
        setInternalState(testee, "success", value);
    }
}