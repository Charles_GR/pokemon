package com.charles.pokemon.view;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static org.apache.commons.lang3.StringUtils.capitalize;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.function.Consumer;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import com.charles.pokemon.R;
import com.charles.pokemon.model.adapters.PokemonStatAdapter;
import com.charles.pokemon.model.data.ComplexPokemon;
import com.charles.pokemon.model.data.SimplePokemon;
import com.charles.pokemon.processing.tasks.FetchOnePokemonTask;

/** Activity which display details of a single Pokemon to the user. */
public final class PokemonDetailsActivity extends Activity
{
    private SimplePokemon simplePokemon;

    private LinearLayout llDetails;
    private ProgressBar pbProgress;
    private ImageView ivSprite;
    private TextView tvNameValue;
    private TextView tvWeightValue;
    private TextView tvHeightValue;
    private TextView tvTypesValue;
    private ListView lvStats;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_details);
        llDetails = findViewById(R.id.llDetails);
        pbProgress = findViewById(R.id.pbProgress);
        ivSprite = findViewById(R.id.ivSprite);
        tvNameValue = findViewById(R.id.tvNameValue);
        tvWeightValue = findViewById(R.id.tvWeightValue);
        tvHeightValue = findViewById(R.id.tvHeightValue);
        tvTypesValue = findViewById(R.id.tvTypesValue);
        lvStats = findViewById(R.id.lvStats);
        simplePokemon = new Gson().fromJson(getIntent().getStringExtra("pokemon"), SimplePokemon.class);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        new FetchOnePokemonTask(simplePokemon.getUrl(), onSuccess, onError).execute();
    }

    private final Consumer<ComplexPokemon> onSuccess = pokemon -> {
        Picasso.get()
                .load(getString(R.string.sprite_url, simplePokemon.extractId()))
                .placeholder(R.mipmap.loading)
                .into(ivSprite);
        tvNameValue.setText(capitalize(pokemon.getName()));
        tvWeightValue.setText(String.valueOf(pokemon.getWeight()));
        tvHeightValue.setText(String.valueOf(pokemon.getHeight()));
        tvTypesValue.setText(TextUtils.join(", ", pokemon.getTypes()));
        lvStats.setAdapter(new PokemonStatAdapter(PokemonDetailsActivity.this, new ArrayList<>(pokemon.getStats().entrySet())));
        pbProgress.setVisibility(GONE);
        llDetails.setVisibility(VISIBLE);
    };

    private final Runnable onError = () ->
    {
        Toast.makeText(PokemonDetailsActivity.this, R.string.fetch_one_error, Toast.LENGTH_LONG).show();
        finish();
    };
}