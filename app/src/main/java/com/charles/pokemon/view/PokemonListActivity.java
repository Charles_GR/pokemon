package com.charles.pokemon.view;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static android.widget.Toast.LENGTH_SHORT;
import static com.charles.pokemon.model.adapters.PokemonAdapter.SortField;
import static com.charles.pokemon.model.adapters.PokemonAdapter.SortOrdering;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import com.google.gson.Gson;

import com.charles.pokemon.R;
import com.charles.pokemon.model.adapters.PokemonAdapter;
import com.charles.pokemon.model.data.SimplePokemon;
import com.charles.pokemon.processing.tasks.FetchAllPokemonTask;

/** Activity which displays a list of Pokemon to the user. */
public final class PokemonListActivity extends Activity implements
        OnClickListener, OnLongClickListener, OnItemClickListener, OnItemSelectedListener, TextWatcher
{
    private Spinner spnSortField;
    private Spinner spnSortOrdering;
    private EditText etSearchQuery;
    private ProgressBar pbProgress;
    private ListView lvPokemon;
    private TextView tvErrorMessage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon_list);
        etSearchQuery = findViewById(R.id.etSearchQuery);
        pbProgress = findViewById(R.id.pbProgress);
        spnSortField = findViewById(R.id.spnSortField);
        spnSortOrdering = findViewById(R.id.spnSortOrdering);
        lvPokemon = findViewById(R.id.lvPokemon);
        tvErrorMessage = findViewById(R.id.tvErrorMessage);
        etSearchQuery.addTextChangedListener(this);
        spnSortField.setAdapter(new ArrayAdapter<>(this, R.layout.spinner_item, SortField.values()));
        spnSortField.setOnItemSelectedListener(this);
        spnSortOrdering.setAdapter(new ArrayAdapter<>(this, R.layout.spinner_item, SortOrdering.values()));
        spnSortOrdering.setOnItemSelectedListener(this);
        lvPokemon.setOnItemClickListener(this);
        setupActionBarWithCustomView();
        refreshPokemon();
    }

    @Override
    public void onClick(View view)
    {
        if (view.getId() == R.id.ivRefresh)
        {
            refreshPokemon();
        }
    }

    @Override
    public boolean onLongClick(View view)
    {
        if (view.getId() == R.id.ivRefresh)
        {
            Toast.makeText(this, R.string.refresh, LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    @Override
    public void onItemClick(AdapterView adapterView, View view, int position, long id)
    {
        SimplePokemon selectedPokemon = (SimplePokemon) lvPokemon.getItemAtPosition(position);
        Intent intent = new Intent(this, PokemonDetailsActivity.class);
        intent.putExtra("pokemon", new Gson().toJson(selectedPokemon));
        startActivity(intent);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
    {
        searchAndSortPokemon();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView)
    {

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
    {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
    {
        searchAndSortPokemon();
    }

    @Override
    public void afterTextChanged(Editable editable)
    {

    }

    private void setupActionBarWithCustomView()
    {
        if (getActionBar() != null && getActionBar().getCustomView() == null)
        {
            View customView = View.inflate(this, R.layout.action_layout_main, null);
            TextView tvTitle = customView.findViewById(R.id.tvTitle);
            ImageView ivRefresh = customView.findViewById(R.id.ivRefresh);
            tvTitle.setText(R.string.app_name);
            ivRefresh.setOnClickListener(this);
            ivRefresh.setOnLongClickListener(this);
            getActionBar().setDisplayShowCustomEnabled(true);
            getActionBar().setCustomView(customView);
        }
    }

    private void searchAndSortPokemon()
    {
        PokemonAdapter pokemonAdapter = (PokemonAdapter) lvPokemon.getAdapter();
        if (pokemonAdapter != null)
        {
            SortField sortField = SortField.valueOf(spnSortField.getSelectedItem().toString().toUpperCase());
            SortOrdering sortOrdering = SortOrdering.valueOf(spnSortOrdering.getSelectedItem().toString().toUpperCase());
            pokemonAdapter.searchItems(etSearchQuery.getText().toString());
            pokemonAdapter.sortItems(sortField, sortOrdering);
        }
    }

    private void refreshPokemon()
    {
        lvPokemon.setAdapter(new PokemonAdapter(this, new ArrayList<>()));
        tvErrorMessage.setVisibility(GONE);
        pbProgress.setVisibility(VISIBLE);
        new FetchAllPokemonTask(fetchPokemonOnSuccess, fetchPokemonOnError).execute();
    }

    private final Consumer<List<SimplePokemon>> fetchPokemonOnSuccess = pokemon ->
    {
        lvPokemon.setAdapter(new PokemonAdapter(PokemonListActivity.this, pokemon));
        searchAndSortPokemon();
        pbProgress.setVisibility(GONE);
    };

    private final Runnable fetchPokemonOnError = () ->
    {
        pbProgress.setVisibility(GONE);
        tvErrorMessage.setVisibility(VISIBLE);
    };
}