package com.charles.pokemon.model.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import com.charles.pokemon.R;

/** Adapter to display a Pokemon statistic as a list item in a ListView. */
public final class PokemonStatAdapter extends ArrayAdapter<Map.Entry<String, Integer>>
{
    public PokemonStatAdapter(Context context, List<Map.Entry<String, Integer>> pokemonStats)
    {
        super(context, R.layout.list_item_pokemon_stat, pokemonStats);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder viewHolder;
        if (convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_pokemon_stat, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvStat = convertView.findViewById(R.id.tvStat);
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Map.Entry<String, Integer> stat = getItem(position);
        viewHolder.tvStat.setText(getContext().getString(R.string.stat,stat.getKey(), stat.getValue()));

        return convertView;
    }

    private static final class ViewHolder
    {
        private TextView tvStat;
    }
}