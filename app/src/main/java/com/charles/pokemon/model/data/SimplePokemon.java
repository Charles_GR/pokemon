package com.charles.pokemon.model.data;

import static java.lang.Integer.parseInt;
import static org.apache.commons.lang3.StringUtils.substringBetween;

/** Simple representation of a Pokemon used when when fetching all Pokemon from PokeAPI. */
public final class SimplePokemon
{
    private final String name;
    private final String url;

    public SimplePokemon(String name, String url)
    {
        this.name = name;
        this.url = url;
    }

    public String getName()
    {
        return name;
    }

    public String getUrl()
    {
        return url;
    }

    public int extractId()
    {
        return parseInt(substringBetween(url, "pokemon/", "/"));
    }
}
