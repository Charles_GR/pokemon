package com.charles.pokemon.model.adapters;

import static com.charles.pokemon.model.adapters.PokemonAdapter.SortOrdering.ASCENDING;
import static java.util.Collections.unmodifiableList;
import static org.apache.commons.lang3.StringUtils.capitalize;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.squareup.picasso.Picasso;

import com.charles.pokemon.R;
import com.charles.pokemon.model.data.SimplePokemon;

/** Adapter to display a SimplePokemon as a list item in a ListView. */
public final class PokemonAdapter extends ArrayAdapter<SimplePokemon>
{
    private final List<SimplePokemon> pokemon;
    private final List<SimplePokemon> filteredPokemon;
    private final PokemonComparator comparator;

    public PokemonAdapter(Context context, List<SimplePokemon> pokemon)
    {
        super(context, R.layout.list_item_pokemon, pokemon);
        this.pokemon = unmodifiableList(new ArrayList<>(pokemon));
        this.filteredPokemon = pokemon;
        this.comparator = new PokemonComparator();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder viewHolder;
        if (convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item_pokemon, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvName = convertView.findViewById(R.id.tvName);
            viewHolder.ivSprite = convertView.findViewById(R.id.ivSprite);
            convertView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        SimplePokemon pokemon = getItem(position);
        viewHolder.tvName.setText(capitalize(pokemon.getName()));
        Picasso.get()
                .load(getContext().getString(R.string.sprite_url, pokemon.extractId()))
                .placeholder(R.mipmap.loading)
                .into(viewHolder.ivSprite);

        return convertView;
    }

    public void sortItems(SortField sortField, SortOrdering sortOrdering)
    {
        comparator.setSortField(sortField);
        comparator.setSortOrdering(sortOrdering);
        filteredPokemon.sort(comparator);
        notifyDataSetChanged();
    }

    public void searchItems(String searchQuery)
    {
        filteredPokemon.clear();
        if (searchQuery.isEmpty())
        {
            filteredPokemon.addAll(pokemon);
        }
        else
        {
            pokemon.stream().filter(pokemon -> pokemon.getName().contains(searchQuery)).forEach(filteredPokemon::add);
        }
        notifyDataSetChanged();
    }

    public enum SortField
    {
        INSERTION("Insertion"),
        NAME("Name");

        private final String label;

        SortField(String label)
        {
            this.label = label;
        }

        @Override
        public String toString()
        {
            return label;
        }
    }

    public enum SortOrdering
    {
        ASCENDING("Ascending"),
        DESCENDING("Descending");

        private final String label;

        SortOrdering(String label)
        {
            this.label = label;
        }

        @Override
        public String toString()
        {
            return label;
        }
    }

    private static final class ViewHolder
    {
        private TextView tvName;
        private ImageView ivSprite;
    }

    private static final class PokemonComparator implements Comparator<SimplePokemon>
    {
        private SortField sortField;
        private SortOrdering sortOrdering;

        @Override
        public int compare(SimplePokemon pokemonA, SimplePokemon pokemonB)
        {
            int compare = 0;
            switch (sortField)
            {
                case INSERTION:
                    compare = Integer.compare(pokemonA.extractId(), pokemonB.extractId());
                    break;
                case NAME:
                    compare = pokemonA.getName().compareTo(pokemonB.getName());
                    break;
            }
            return sortOrdering.equals(ASCENDING) ? compare : -compare;
        }

        public void setSortField(SortField sortField)
        {
            this.sortField = sortField;
        }

        private void setSortOrdering(SortOrdering sortOrdering)
        {
            this.sortOrdering = sortOrdering;
        }
    }
}