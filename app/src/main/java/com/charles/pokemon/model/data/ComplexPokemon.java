package com.charles.pokemon.model.data;

import java.util.List;
import java.util.Map;

/** Complex representation of a Pokemon used when when fetching a single Pokemon from PokeAPI. */
public final class ComplexPokemon
{
    private final String name;
    private final float weight;
    private final int height;
    private final List<String> types;
    private final Map<String, Integer> stats;

    public ComplexPokemon(String name, float weight, int height, List<String> types, Map<String, Integer> stats)
    {
        this.name = name;
        this.weight = weight;
        this.height = height;
        this.types = types;
        this.stats = stats;
    }

    public String getName()
    {
        return name;
    }

    public float getWeight()
    {
        return weight;
    }

    public int getHeight()
    {
        return height;
    }

    public List<String> getTypes()
    {
        return types;
    }

    public Map<String, Integer> getStats()
    {
        return stats;
    }
}
