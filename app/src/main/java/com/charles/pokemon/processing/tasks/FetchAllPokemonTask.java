package com.charles.pokemon.processing.tasks;

import static com.charles.pokemon.processing.util.JsonFetcher.fetchJson;
import static com.charles.pokemon.processing.util.PokemonParser.parseAllPokemon;
import static java.util.Collections.emptyList;

import android.os.AsyncTask;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

import com.charles.pokemon.model.data.SimplePokemon;

/** AsyncTask to carry out background task of fetching all Pokemon from PokeAPI and parsing into a list of SimplePokemon. */
public final class FetchAllPokemonTask extends AsyncTask<Void, Void, List<SimplePokemon>>
{
    private static final String JSON_URL = "https://pokeapi.co/api/v2/pokemon?limit=300";

    private final Consumer<List<SimplePokemon>> onSuccess;
    private final Runnable onError;
    private boolean success;

    public FetchAllPokemonTask(Consumer<List<SimplePokemon>> onSuccess, Runnable onError)
    {
        this.onSuccess = onSuccess;
        this.onError = onError;
        this.success = true;
    }

    @Override
    protected List<SimplePokemon> doInBackground(Void... voids)
    {
        try
        {
            return parseAllPokemon(fetchJson(JSON_URL));
        }
        catch (IOException e)
        {
            success = false;
            return emptyList();
        }
    }

    @Override
    protected void onPostExecute(List<SimplePokemon> pokemon)
    {
        if (success)
        {
            onSuccess.accept(pokemon);
        }
        else
        {
            onError.run();
        }
    }
}