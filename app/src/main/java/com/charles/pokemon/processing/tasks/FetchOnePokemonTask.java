package com.charles.pokemon.processing.tasks;

import static com.charles.pokemon.processing.util.JsonFetcher.fetchJson;
import static com.charles.pokemon.processing.util.PokemonParser.parseOnePokemon;

import android.os.AsyncTask;

import java.io.IOException;
import java.util.function.Consumer;

import com.charles.pokemon.model.data.ComplexPokemon;

/** AsyncTask to carry out background task of fetching one Pokemon from PokeAPI and parsing into a ComplexPokemon. */
public final class FetchOnePokemonTask extends AsyncTask<Void, Void, ComplexPokemon>
{
    private final String pokemonUrl;
    private final Consumer<ComplexPokemon> onSuccess;
    private final Runnable onError;
    private boolean success;

    public FetchOnePokemonTask(String pokemonUrl, Consumer<ComplexPokemon> onSuccess, Runnable onError)
    {
        this.pokemonUrl = pokemonUrl;
        this.onSuccess = onSuccess;
        this.onError = onError;
        this.success = true;
    }

    @Override
    protected ComplexPokemon doInBackground(Void... voids)
    {
        try
        {
            return parseOnePokemon(fetchJson(pokemonUrl));
        }
        catch (IOException e)
        {
            success = false;
            return null;
        }
    }

    @Override
    protected void onPostExecute(ComplexPokemon pokemon)
    {
        if (success)
        {
            onSuccess.accept(pokemon);
        }
        else
        {
            onError.run();
        }
    }
}