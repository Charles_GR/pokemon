package com.charles.pokemon.processing.util;

import java.io.IOException;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

/** Util class to fetch and return JSON from GET request to given URL. */
public final class JsonFetcher
{
    private static final OkHttpClient CLIENT = new OkHttpClient();

    public static String fetchJson(String url) throws IOException
    {
        Request request = new Request.Builder().url(url).build();
        Response response = CLIENT.newCall(request).execute();

        if (response.code() == 200)
        {
            return response.body().string();
        }
        else
        {
            throw new IOException();
        }
    }
}
