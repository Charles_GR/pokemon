package com.charles.pokemon.processing.util;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.StreamSupport.stream;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import com.charles.pokemon.model.data.ComplexPokemon;
import com.charles.pokemon.model.data.SimplePokemon;

/** Util class to parse JSON into either a single Pokemon or a list of Pokemon. */
public final class PokemonParser
{
    private static final Gson GSON = new Gson();

    public static List<SimplePokemon> parseAllPokemon(String json)
    {
        JsonArray pokemon = GSON.fromJson(json, JsonObject.class).get("results").getAsJsonArray();
        return stream(pokemon.spliterator(), false)
                .map(aPokemon -> GSON.fromJson(aPokemon, SimplePokemon.class))
                .collect(toList());
    }

    public static ComplexPokemon parseOnePokemon(String json)
    {
        JsonObject pokemon = GSON.fromJson(json, JsonObject.class);
        return new ComplexPokemon(pokemon.get("name").getAsString(),
                pokemon.get("weight").getAsFloat() / 10,
                pokemon.get("height").getAsInt() * 10,
                parsePokemonTypes(pokemon.get("types").getAsJsonArray().toString()),
                parsePokemonStats(pokemon.get("stats").getAsJsonArray().toString()));
    }

    private static List<String> parsePokemonTypes(String json)
    {
        JsonArray types = JsonParser.parseString(json).getAsJsonArray();
        return stream(types.spliterator(), false)
                .map(type -> type.getAsJsonObject().get("type").getAsJsonObject().get("name").getAsString())
                .collect(toList());
    }

    private static Map<String, Integer> parsePokemonStats(String json)
    {
        JsonArray stats = JsonParser.parseString(json).getAsJsonArray();
        return stream(stats.spliterator(), false)
                .collect(toMap(stat -> stat.getAsJsonObject().get("stat").getAsJsonObject().get("name").getAsString(),
                        stat -> stat.getAsJsonObject().get("base_stat").getAsInt(),
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
    }
}
